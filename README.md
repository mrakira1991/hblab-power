# HBLAB Power

## Description
A virtual power plant system for aggregating distributed power sources into a single cloud-based energy provider

## Usage
<p>1/ Run the application by main class PowerApplication.java</p> 
<p>2/ You can test the API with the context path <a href="http://localhost:8080/api/">http://localhost:8080/api/</a></p>
Or you can use swagger ui by access the url <a href="http://localhost:8080/api/swagger-ui/index.html#/">http://localhost:8080/api/swagger-ui/index.html#/</a> 

<img src="docs/swagger-ui.jpg" />

## Testing
<p> 1/ You can test create many batteries with the API /batteries/batch</p>
<img src="docs/create-api.jpg" />

<p> Execute the API and response status will be 201 Created </p>
<img src="docs/create-ok.jpg" />

<p> 2/ Test the GET API to search the batteries within the postcode range</p>
<p> The API will receives a postcode range.The response body will contain a list of names of batteries that fall within the range, sorted alphabetically. Additionally, there should be some statistics included for the returned batteries, such as total and average watt capacity. </p>

<img src="docs/get-api.jpg" />
<img src="docs/get-ok.jpg" />
