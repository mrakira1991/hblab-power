package com.example.power.service.impl;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.power.domain.Battery;
import com.example.power.dto.filter.BatteryFilter;
import com.example.power.dto.request.BatteryRequest;
import com.example.power.dto.response.BatteryResponse;
import com.example.power.dto.response.BatteryStatisticResponse;
import com.example.power.repository.BatteryRepository;

@SpringBootTest
public class DefaultBatteryServiceImplTest {

    @Autowired
    private BatteryRepository batteryRepository;

    @Autowired
    private DefaultBatteryServiceImpl batteryService;

    @Test
    public void batchCreate_shouldWork() {
        batteryService.batchCreate(List.of(createBatteryRequest("c", 123456, 1D), createBatteryRequest("b", 456, 2D)));
        List<Battery> batteries = batteryRepository.findAll();

        Assertions.assertEquals(2, batteries.size());

        Assertions.assertEquals("c", batteries.get(0).getName());
        Assertions.assertEquals((Integer) 123456, batteries.get(0).getPostcode());
        Assertions.assertEquals(1D, batteries.get(0).getWattCapacity(), 0.001);

        Assertions.assertEquals("b", batteries.get(1).getName());
        Assertions.assertEquals((Integer) 456, batteries.get(1).getPostcode());
        Assertions.assertEquals(2D, batteries.get(1).getWattCapacity(), 0.001);
    }

    @Test
    public void find_whenFilterHasOnlyMinPostcode_shouldReturnCorrectly() {
        batteryService.batchCreate(List.of(
        		createBatteryRequest("c", 123, 1D),
                createBatteryRequest("b", 123456, 2D),
                createBatteryRequest("a", 456789, 3D)));

        BatteryFilter filter = BatteryFilter.builder()
        	.minPostcode(123111)
        	.build();
        BatteryStatisticResponse result = batteryService.find(filter);

        List<BatteryResponse> batteries = result.getBatteries();

        Assertions.assertEquals(2, batteries.size());
        Assertions.assertEquals("a", batteries.get(0).getName());

        Assertions.assertEquals(5.0, result.getTotalWattCapacity());
        Assertions.assertEquals(2.5, result.getAverageWattCapacity());
    }
    
    @Test
    public void find_whenFilterHasOnlyMaxPostcode_shouldReturnCorrectly() {
        batteryService.batchCreate(List.of(
        		createBatteryRequest("c", 123, 1D),
                createBatteryRequest("b", 123456, 2D),
                createBatteryRequest("a", 456789, 3D)));

        BatteryFilter filter = BatteryFilter.builder()
        	.maxPostcode(123457)
        	.build();
        BatteryStatisticResponse result = batteryService.find(filter);

        List<BatteryResponse> batteries = result.getBatteries();

        Assertions.assertEquals(2, batteries.size());
        Assertions.assertEquals("b", batteries.get(0).getName());

        Assertions.assertEquals(3.0, result.getTotalWattCapacity());
        Assertions.assertEquals(1.5, result.getAverageWattCapacity());
    }
    
    @Test
    public void find_whenHasFilterPostcode_shouldReturnCorrectly() {
        batteryService.batchCreate(List.of(
        		createBatteryRequest("c", 123, 1D),
                createBatteryRequest("b", 123456, 2D),
                createBatteryRequest("a", 456789, 3D)));

        BatteryFilter filter = BatteryFilter.builder()
        	.minPostcode(123111)
        	.maxPostcode(123555)
        	.build();
        BatteryStatisticResponse result = batteryService.find(filter);

        List<BatteryResponse> batteries = result.getBatteries();

        Assertions.assertEquals(1, batteries.size());
        Assertions.assertEquals("b", batteries.get(0).getName());

        Assertions.assertEquals(2.0, result.getTotalWattCapacity());
        Assertions.assertEquals(2.0, result.getAverageWattCapacity());
    }

    @Test
    public void find_whenNoFilter_thenReturnAll() {
        batteryService.batchCreate(List.of(
        		createBatteryRequest("c", 123, 1D),
                createBatteryRequest("b", 123456, 2D),
                createBatteryRequest("a", 456789, 3D)));

        BatteryFilter filter = BatteryFilter.builder().build();
        BatteryStatisticResponse result = batteryService.find(filter);

        List<BatteryResponse> batteries = result.getBatteries();

        Assertions.assertEquals(3, batteries.size());
        Assertions.assertEquals("a", batteries.get(0).getName());

        Assertions.assertEquals(6.0, result.getTotalWattCapacity());
        Assertions.assertEquals(2.0, result.getAverageWattCapacity());
        
		// Check case filter is null
		result = batteryService.find(null);
		batteries = result.getBatteries();

		Assertions.assertEquals(3, batteries.size());
        Assertions.assertEquals("a", batteries.get(0).getName());

        Assertions.assertEquals(6.0, result.getTotalWattCapacity());
        Assertions.assertEquals(2.0, result.getAverageWattCapacity());
    }
    
    private BatteryRequest createBatteryRequest(String name, Integer postcode, Double wattCapacity) {
        return new BatteryRequest(null, name, postcode, wattCapacity);
    }

    @AfterEach
    public void tearDown() {
        batteryRepository.deleteAll();
    }
}
