package com.example.power.dto.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BatteryRequest {

	/**
	 * For update purpose
	 */
	private Long id;

	@NotBlank(message = "Name is mandatory")
	private String name;

	@Min(value = 1, message = "must be equal or greater than 1")
	@NotNull(message = "Postcode is mandatory")
	private Integer postcode;

	@Min(value = 1, message = "must be equal or greater than 1")
	@NotNull(message = "Watt Capacity is mandatory")
	private Double wattCapacity;

}
