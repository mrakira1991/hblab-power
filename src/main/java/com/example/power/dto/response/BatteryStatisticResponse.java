package com.example.power.dto.response;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BatteryStatisticResponse {

	private List<BatteryResponse> batteries;
	private Double totalWattCapacity;
	private Double averageWattCapacity;

}
