package com.example.power.dto.response;

import lombok.Data;

@Data
public class BatteryResponse {

    private Long id;
    private String name;
    private Integer postcode;
    private Double wattCapacity;

}
