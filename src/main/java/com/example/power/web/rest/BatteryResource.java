package com.example.power.web.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.power.constants.ApiConstant;
import com.example.power.dto.filter.BatteryFilter;
import com.example.power.dto.request.BatteryRequest;
import com.example.power.dto.response.BatteryResponse;
import com.example.power.dto.response.BatteryStatisticResponse;
import com.example.power.service.BatteryService;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping(ApiConstant.BATTERY_URL)
public class BatteryResource {

    private final BatteryService batteryService;

	@Operation(summary = "Get all batteries")
    @GetMapping
    public BatteryStatisticResponse find(BatteryFilter filter) {
        return batteryService.find(filter);
    }

	@Operation(summary = "Create batch batteries")
    @PostMapping("/batch")
    @ResponseStatus(code = HttpStatus.CREATED)
    public List<BatteryResponse> batchCreate(@Valid @RequestBody List<BatteryRequest> requests) {
        return batteryService.batchCreate(requests);
    }

}
