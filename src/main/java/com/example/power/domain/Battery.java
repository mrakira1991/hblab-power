package com.example.power.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "tbl_battery")
@Data
public class Battery {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "postcode", nullable = false)
    private Integer postcode;

    @Column(name = "watt_capacity", nullable = false)
    private Double wattCapacity;
}
