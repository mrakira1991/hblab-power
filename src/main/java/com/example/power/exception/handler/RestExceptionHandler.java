package com.example.power.exception.handler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.power.dto.response.ApiErrorResponse;


@RestControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public ResponseEntity<ApiErrorResponse> handleResourceNotFoundException(ConstraintViolationException ex,
			HttpServletRequest request) {
		//
		ApiErrorResponse response = new ApiErrorResponse();
		response.setCode(HttpStatus.BAD_REQUEST.value());
		response.setMessage(ex.getMessage());
		response.setPath(request.getContextPath() + request.getServletPath());

		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}
}
