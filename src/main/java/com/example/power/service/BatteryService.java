package com.example.power.service;

import java.util.List;

import com.example.power.dto.filter.BatteryFilter;
import com.example.power.dto.request.BatteryRequest;
import com.example.power.dto.response.BatteryResponse;
import com.example.power.dto.response.BatteryStatisticResponse;

public interface BatteryService {

	BatteryStatisticResponse find(BatteryFilter filter);

	List<BatteryResponse> batchCreate(List<BatteryRequest> requests);

}
