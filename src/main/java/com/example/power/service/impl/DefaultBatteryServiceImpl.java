package com.example.power.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.example.power.constants.DbConstant;
import com.example.power.domain.Battery;
import com.example.power.dto.filter.BatteryFilter;
import com.example.power.dto.request.BatteryRequest;
import com.example.power.dto.response.BatteryResponse;
import com.example.power.dto.response.BatteryStatisticResponse;
import com.example.power.repository.BatteryRepository;
import com.example.power.service.BatteryService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DefaultBatteryServiceImpl implements BatteryService {

    private final BatteryRepository batteryRepository;

    @Override
    public List<BatteryResponse> batchCreate(List<BatteryRequest> requests) {
        return requests.stream()
                //Rely to jpa
                .map(this::create)
                .collect(Collectors.toList());
    }

    @Override
    public BatteryStatisticResponse find(BatteryFilter filter) {
        List<BatteryResponse> batteryResponses = batteryRepository.findAll(buildSpecificationByFilter(filter))
        		.stream().map(this::toResponse)
        		.sorted(Comparator.comparing(BatteryResponse::getName))
                .collect(Collectors.toList());

        return BatteryStatisticResponse.builder()
        		.batteries(batteryResponses)
        		.totalWattCapacity(calculateTotalWattCapacity(batteryResponses))
        		.averageWattCapacity(calculateAverageWattCapacity(batteryResponses))
        		.build();

    }
    
    private BatteryResponse create(BatteryRequest request) {
        return toResponse(batteryRepository.save(toEntity(request)));
    }
    
    private Double calculateTotalWattCapacity(List<BatteryResponse> batteryResponses) {
    	return batteryResponses.stream().mapToDouble(BatteryResponse::getWattCapacity).sum();
    }
    
    private Battery toEntity(BatteryRequest request) {
        Battery entity = new Battery();
        BeanUtils.copyProperties(request, entity, "id");

        return entity;
    }

    private BatteryResponse toResponse(Battery entity) {
        BatteryResponse response = new BatteryResponse();
        BeanUtils.copyProperties(entity, response);

        return response;
    }
    
    private Double calculateAverageWattCapacity(List<BatteryResponse> batteryResponses) {
    	return batteryResponses.stream().mapToDouble(BatteryResponse::getWattCapacity).average().orElse(0D);
    }
    
    private Specification<Battery> buildSpecificationByFilter(BatteryFilter filter) {
    	if (filter == null) {
    		return (root, query, cb) -> cb.conjunction();
    	}
    	
    	return (root, query, cb) -> {
    		List<Predicate> predicates = new ArrayList<>();
    		if (filter.getMinPostcode() != null ) {
        		predicates.add(cb.greaterThanOrEqualTo(root.get(DbConstant.BATTERY_POSTCODE_COLUMN_NAME), filter.getMinPostcode()));
        	}
    		
    		if (filter.getMaxPostcode() != null) {
    			predicates.add(cb.lessThanOrEqualTo(root.get(DbConstant.BATTERY_POSTCODE_COLUMN_NAME), filter.getMaxPostcode()));
        	}
    		
    		return cb.and(predicates.toArray(new Predicate[] {}));
    	};
    }
}
